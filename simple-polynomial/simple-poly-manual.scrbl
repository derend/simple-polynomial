#lang scribble/manual
@(require scribble/example
          racket/draw
          (for-label racket plot
                     "main.rkt" "tools.rkt" "fit.rkt"))
@(define (author-email) "deren.dohoda@gmail.com")

@title[#:tag "top"]{Simple Polynomials of One Variable}
@author{@(author+email "Deren Dohoda" (author-email))}

@(define this-eval (let ((e (make-base-eval)))
                     (e '(begin (require "main.rkt" plot/pict)))
                     e))

@;;;;;;;;;;;;;;;;;;;;;;;;
@;;;;;;;;;;;;;;;;;;;;;;;;
@;;;;;;;;;;;;;;;;;;;;;;;; 
@section{A Simple Polynomial Library}
@defmodule[simple-polynomial]
This library provides some basic tools for working with polynomials of one variable.

@;;;;;;;;;;;;;;;;;;;;;;;;
@;;;;;;;;;;;;;;;;;;;;;;;;
@;;;;;;;;;;;;;;;;;;;;;;;;

@section{Polynomial Creation, Extraction, and Checking}
@defmodule[simple-polynomial/base]
@defproc[(coefficient? [maybe-n any/c]) boolean?]{Determines whether a given value is acceptable as a polynomial
 coefficient. Racket considers floating point exceptional "numbers" @racket[(list +nan.0 -nan.0 +inf.0 -inf.0)]
 as values which qualify as @racket[number?]s but they will not be accepted as coefficients by this library.}
@deftogether[(@defstruct*[polynomial ([terms (listof coefficient?)])]
               @defproc[(poly [coefficient coefficient?] ...) polynomial?])]{Yields a polynomial with the given
 coefficients. The leftmost coefficient is the most significant, so for instance
 @racketblock[(polynomial '(2 1))] would represent the line 2@italic{x}+1. The resulting polynomial is a procedure
 which accepts a single @racket[(or/c coefficient? polynomial?)] and evaluates the polynomial at that value.}
@examples[#:eval this-eval
          (define P (poly 2 -1 1))
          (P 2)]
@defproc[(poly? [p any/c]) boolean?]{Short for @racket[polynomial?], determines whether the given argument is a
 polynomial in the sense of this library.}
@defproc[(poly= [p1 polynomial?] [p2 polynomial?]) boolean?]{Equality between two polynomials is determined by
 agreement on their coefficients.}

@subsection{Display of Polynomials}
Polynomials are custom printed in a latex-like format, surrounded by greater than and less than symbols,
with the variable @code{x}, and exponents wrapped in curly-braces, like @racketblock{<2x^{2} + 3>}
@defparam[display-short? boolean? boolean? #:value #f]{Polynomials in calculations can grow to many terms, so
 this parameter determines whether polynomials will be shortened for interactive purposes. A shortened polynomial
 will be truncated in the middle with some ellipses.}
@examples[#:eval this-eval 
          (define P (poly 1 2 3 4 5 6 7))
          P
          (display-short? #t)
          P]
@examples[#:eval this-eval 
          #:hidden (display-short? #f)]
@defproc[(poly->string [p polynomial?]) string?]{Converts the given polynomial to a string, exactly as it displays
 interactively, except that the value of the parameter @racket[display-short?] is ignored.}
@defproc[(string->poly [s string?]) polynomial?]{Interprets a string written in the same format as a polynomial.
 Although this library will not show coefficients of 1 or exponents of 1, they can be read. Additionally, the "<"
 and ">" brackets are not required. However, if present all exponents must be wrapped in curly braces. Finally,
 like terms in the string will be combined.}
@examples[#:eval this-eval 
          (define P (poly 1 2 3 4 5 6 7))
          (display-short? #t)
          P
          (poly->string P)
          (string->poly "x+1")
          (string->poly "x - 2x^{2} + 1 + x + 1")]
@examples[#:eval this-eval 
          #:hidden (display-short? #f)]
@defproc[(poly->latex [p polynomial?] [#:inexact-digits id number? 4]) string?]{Converts the given polynomial
 into a latex-suitable string. If the coefficients of the polynomial are @racket[inexact?] then
 @racket[#:inexact-digits] controls how many digits will print in scientific notation. The output will be
 mixed between @racket[exact?] and @racket[inexact?] representations if the input was mixed.}
@examples[#:eval this-eval 
          (define P (poly 1/2 1 1.5 1.334567))
          (poly->latex P)
          (poly->latex P #:inexact-digits 2)]

@subsection{Arithmetic}
@deftogether[(@defproc[(poly+ [p (or/c coefficient? polynomial?)] ...) polynomial?]
               @defproc[(poly- [p (or/c coefficient? polynomial?)] ...) polynomial?]
               @defproc[(poly* [p (or/c coefficient? polynomial?)] ...) polynomial?])]{Ordinary arithmetic with
 polynomials. In this library, Racket numbers are interpreted as degenerate polynomials. Each procedure is variadic,
 which for addition and multiplication is of no consequence since these are commutative operations. Subtraction is
 then like Racket subtraction, so @racketblock[(poly- p1 p2 p3)] is like @italic{p1 - p2 - p3}.}
@examples[#:eval this-eval 
          (define p1 (poly 2 -2 12 1))
          (poly+ p1 p1)
          (poly* p1 p1)
          (poly* 2 p1)
          (poly+ 1 1 1)]
@defproc[(poly-expt [base polynomial?]
                    [exponent nonnegative-integer?]) polynomial?]{Repeated multiplication of polynomials.}
@defproc[(poly-quotient/remainder [num polynomial?]
                                  [den polynomial?])
         (values polynomial? polynomial?)]{Similar to @racket[quotient/remainder], except on polynomials.}
@defproc[(poly-gcd [left polynomial?]
                   [right polynomial?])
         polynomial?]{Computes the largest polynomial that divides both @code{left} and @code{right} with
 remainder 0.}

@;;;;;;;;;;;;;;;;;;;;;;;;
@;;;;;;;;;;;;;;;;;;;;;;;;
@;;;;;;;;;;;;;;;;;;;;;;;;

@section{Additional Tools}
@defmodule[simple-polynomial/tools]
@defproc[(points->polynomial [pts (listof (listof number?))]) polynomial?]{Takes a list of points, represented
 as a list or cons of @code{x,y} terms, and yields the polynomial determined by these points.}
@margin-note{If the desired starting point is not at 0 but at @code{x=a}, simply compose the resulting polynomial
 with @racket[(poly 1 (- a))].}
@defproc[(interpolate-at-integer-points [y-values (listof number?)])
         polynomial?]{Like @racket[points->polynomial], but assumes x-values are sequential integers starting from
 0. Faster than @racket[points->polynomial] if the points are in this format.}
@defproc[(rational-roots [p polynomial?]) (listof number?)]{Finds all rational roots of the given polynomial.}
@defproc[(falling-factorial [n nonnegative-integer?]) polynomial?]{Calculates @code{x(x-1)(x-2)...(x-n+1)}.}
@defproc[(rising-factorial [n nonnegative-integer?]) polynomial?]{Calculates @code{x(x+1)(x+2)...(x+n-1)}.}

@;;;;;;;;;;;;;;;;;;;;;;;;
@;;;;;;;;;;;;;;;;;;;;;;;;
@;;;;;;;;;;;;;;;;;;;;;;;;

@section{Fitting Points}
@defmodule[simple-polynomial/fit]
@margin-note{A cubic spline would join every pair of points with a cubic equation, and where the cubic equations
 join at an interior point they would agree on slope and curvature at that joining point. But this still leaves
 the entire spline underdetermined because the first and last points have no further joins. "Natural" cubic
 splines set this curvature to zero.}
@defproc[(points->spline [pts (and/c (listof (listof coefficient?))
                                     (>/c (length pts) 2))])
         (listof polynomial?)]{Creates a natural cubic spline for the given points.}
@defproc[(points->spline-evaluator [pts (and/c (listof (listof coefficient?))
                                     (>/c (length pts) 2))])
         procedure?]{Creates an interpolation function from the given points. Some consideration is given to make
 this evaluation faster for sequential operations like plotting.}
@examples[#:eval this-eval 
          (define pts '((1 1) (2 3) (3 -1) (4 1/2)))
          (define peval (points->spline-evaluator pts))
          (peval 1/2)
          (peval 0.5)
          (peval 3)]
@defproc[(points->best-fit-polynomial [pts (listof (listof coefficient?))]
                                      [deg positive-integer?])
         polynomial?]{Creates a least-squares approximation polynomial of the given degree. The degree must be at
 least one larger than the number of points given; if the degree is exactly one more than the number of points
 given, the fit is exact. If the points can be explained by a polynomial of equal or lesser degree, the fit
 will be exact.}
@examples[#:eval this-eval 
          (define pts (for/list ((x (in-range 6))
                                 (noise '(1/9 -1/7 0 1/3 -1 -1/9)))
                        (list x (+ x noise))))
          (define P (points->best-fit-polynomial pts 1))
          (plot (list (points pts)
                      (function P -0.5 5.5 #:label (poly->string P)))
                #:width 600 #:height 480
                #:legend-anchor 'bottom-right)
          (define P2 (points->best-fit-polynomial pts 2))
          (plot (list (points pts)
                      (function P2 -0.5 5.5 #:label (poly->string P2)))
                #:width 600 #:height 480
                #:legend-anchor 'bottom-right)]
@margin-note{For the background of this, see the paper "General Least-Squares Smoothing and Differentiation
             by the Convolution (Savistzky-Golay) Method". Gorry, P. Anal. Chem. 1990, 62, 570-573.}
@defproc[(make-least-squares-point-smoother [width number?]
                                            [order number?]
                                            [derivative-order number?])
         procedure?]{Produces a function which smooths points based on a moving window of @racket[width] points.
 The smoothing function is a least squares polynomial of degree @racket[order]. A numerical derivative of the
 points is chosen by @racket[derivative-order]. The window @racket[width] should be larger than the number of points
 to be smoothed by the resulting procedure, and the @racket[derivative-order] must be greater than or equal
 to the smoothing @racket[order]; an argument error will be raised upon application of the resulting
 procedure if either of these conditions is violated by the points used.

 Here is an example image of some real-world data looked at raw, smoothed, and with smoothed first and second order
 derivatives. @(read-bitmap "smooth-example.png")
 The smoothing of the main interpolation is somewhat masked by the scale, so here is a closeup of the unsmoothed
 and smoothed interpolations. @(read-bitmap "smooth-example-closeup.png")}
